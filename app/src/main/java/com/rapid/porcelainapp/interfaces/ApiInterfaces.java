package com.rapid.porcelainapp.interfaces;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface ApiInterfaces {

    @GET("weather")
    Call<ResponseBody> getWeather(@Query("q") String q, @Query("appid") String appid);


}

