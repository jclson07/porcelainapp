package com.rapid.porcelainapp.module;

import com.rapid.porcelainapp.interfaces.ApiInterfaces;
import com.rapid.porcelainapp.network.ApiService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


@Module(includes = {NetworkModule.class, GsonModule.class})
public class ApiServiceModule {


        public static final String BASE_URL ="https://samples.openweathermap.org/data/2.5/";
        public static final String IMAGE_URL = "https://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b6907d289e10d714a6e88b30761fae22";

        // Live IP
        //  public static final String BASE_URL ="http://52.8.60.195/api/v1/";
        //  public static final String IMAGE_URL = "http://52.8.60.195/";
        private static ApiServiceModule mInstance;

        @Provides
        @Singleton
        public ApiService getApiService(Retrofit retrofit) {
            return retrofit.create(ApiService.class);
        }

        @Provides
        @Singleton
        public Retrofit getRetrofit() {
                  OkHttpClient okHttpClient = new OkHttpClient.Builder()
                        .build();
                 return new Retrofit.Builder()
                         .client(okHttpClient)
                         .addConverterFactory(GsonConverterFactory.create())
                         .baseUrl(BASE_URL)
                         .build();
        }
        public static synchronized ApiServiceModule getInstance(){
          if(mInstance == null){
                 mInstance = new ApiServiceModule();
            }
             return mInstance;
        }
        public ApiInterfaces getAPI(){
                return getRetrofit().create(ApiInterfaces.class);
        }

}

