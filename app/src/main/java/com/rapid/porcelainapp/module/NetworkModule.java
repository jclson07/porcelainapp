package com.rapid.porcelainapp.module;

import android.content.Context;
import android.util.Log;

import java.io.File;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;


@Module(includes = ContextModule.class)
public class NetworkModule {

    @Provides
    @Singleton
    public File getCacheFile(Context context) {
        return new File(context.getCacheDir(), "okhttp_cache");
    }

    @Provides
    @Singleton
    public Cache getCache(File cacheFile) {
        return new Cache(cacheFile, 10 * 1000 * 1000); //10 MB Cache
    }

    @Provides
    @Singleton
    public HttpLoggingInterceptor getHttpLoggingInterceptor() {

        return new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                Log.i(NetworkModule.class.getName(), message);
            }
        }).setLevel(HttpLoggingInterceptor.Level.BASIC);

    }


    @Provides
    @Singleton
    public OkHttpClient getOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor, Cache cache) {

        return new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .cache(cache)
                .build();

    }

}
