package com.rapid.porcelainapp.component;

import com.rapid.porcelainapp.module.ApiServiceModule;
import com.rapid.porcelainapp.module.PicassoModule;
import com.rapid.porcelainapp.network.ApiService;
import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApiServiceModule.class, PicassoModule.class})
public interface AppControllerComponent {

    ApiService getGoogleApiService();
    Picasso getPicasso();
}