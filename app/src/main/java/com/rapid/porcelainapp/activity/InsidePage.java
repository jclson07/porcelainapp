package com.rapid.porcelainapp.activity;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.os.Bundle;
import android.widget.TextView;

import com.rapid.porcelainapp.R;

public class InsidePage extends AppCompatActivity {

    @BindView(R.id.tv_title)
    TextView tvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inside_page);
        ButterKnife.bind(this);

        tvTitle.setText(getIntent().getStringExtra("title"));
    }
    @OnClick(R.id.iv_back)
    void onBackClick(){
        finish();
    }
}
