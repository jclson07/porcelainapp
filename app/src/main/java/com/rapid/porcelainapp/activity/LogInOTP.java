package com.rapid.porcelainapp.activity;


import androidx.annotation.BinderThread;
import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.rapid.porcelainapp.R;

public class LogInOTP extends AppCompatActivity implements TextView.OnEditorActionListener {

    EditText etPhone;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in_otp);
        etPhone = findViewById(R.id.et_phone);
        etPhone.setImeOptions(EditorInfo.IME_ACTION_DONE);
        etPhone.setOnEditorActionListener(this);
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        Intent i = new Intent(this, DashBoardPage.class);
        startActivity(i);
        this.finish();
        return false;
    }
}
