package com.rapid.porcelainapp.activity;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

//import com.google.android.gms.auth.api.Auth;
import com.facebook.CallbackManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.HintRequest;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.rapid.porcelainapp.R;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginPage extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks {

    @BindView(R.id.et_phone)
    EditText etPhone;
    CustomTextWatcher obj;

    @BindView(R.id.fl_google)
    FrameLayout flGoogle;
    GoogleApiClient googleApiClient;
    GoogleSignInResult result;
    private static final int RC_SIGN_IN = 1;
    private CallbackManager callbackManager;
    GoogleApiClient mGoogleApiClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        setContentView(R.layout.activity_log_in_page);
        ButterKnife.bind(this);
        callbackManager = CallbackManager.Factory.create();

        obj= new CustomTextWatcher();
        etPhone.addTextChangedListener(obj);

        GoogleSignInOptions gso =  new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken("257866145551-jb6cj0ar4doko43p79sdlak80p1hv3jk.apps.googleusercontent.com")
                .requestEmail()
                .build();
            googleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                   .build();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==RC_SIGN_IN){
            result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);

        }else{
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }
    private void handleSignInResult(GoogleSignInResult result){
        if(result.isSuccess()){
            GoogleSignInAccount account = result.getSignInAccount();
            String email = account.getEmail();
            String first_name = account.getGivenName();
            String last_name = account.getFamilyName();
            String uri = account.getPhotoUrl().toString();
            String login_type = "1";
            Toast.makeText(getApplicationContext(),"Email: "+email+"\n FirstName: "+first_name+"\nLastName: "+last_name+"\nPhoto URL"+uri,Toast.LENGTH_LONG).show();
            Intent i = new Intent(this, DashBoardPage.class);
            startActivity(i);
            this.finish();
        }else{
            Toast.makeText(getApplicationContext(),"Sign in cancel",Toast.LENGTH_LONG).show();
        }
    }

    @OnClick(R.id.fl_google)
    void onGoogleLogInClick(){
           Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
            startActivityForResult(intent,RC_SIGN_IN);
    }
    @OnClick(R.id.fl_fblogin)
    void onFBLogInClick(){
        Intent i = new Intent(this, LogInOTP.class);
        startActivity(i);
        this.finish();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    public void getHintPhoneNumber() {
        HintRequest hintRequest =
                new HintRequest.Builder()
                        .setPhoneNumberIdentifierSupported(true)
                        .build();
        PendingIntent mIntent = Auth.CredentialsApi.getHintPickerIntent(mGoogleApiClient, hintRequest);
        try {
            startIntentSenderForResult(mIntent.getIntentSender(), 101, null, 0, 0, 0);
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }
    }


    class CustomTextWatcher implements TextWatcher {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            // TODO Auto-generated method stub
        }

        @Override
        public void afterTextChanged(Editable s) {
            // TODO Auto-generated method stub
            String initial = s.toString();
            // remove all non-digits characters
            String processed = initial.replaceAll("\\D", "");

            // insert a space after all groups of 4 digits that are followed by another digit
            processed = processed.replaceAll("(?:^|\\D)(\\d{3})[)\\-. ]*?(\\d{3})[\\-. ]*?(\\d{4})", "$1 $2 $3");
            //   processed = processed.replaceAll("(^\\(d{3}\\)d{3}-d{4}$)", "$1 $2 $3 ");(?:^|\D)(\d{3})[)\-. ]*?(\d{3})[\-. ]*?(\d{4})(?:$|\D)

            //Remove the listener
            etPhone.removeTextChangedListener(this);

            //Assign processed text
            etPhone.setText(processed);
            try {
                etPhone.setSelection(processed.length());
            } catch (Exception e) {
                // TODO: handle exception
            }
            //Give back the listener
            etPhone.addTextChangedListener(this);

        }


    }

}
