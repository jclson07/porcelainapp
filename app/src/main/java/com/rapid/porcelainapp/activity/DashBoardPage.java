package com.rapid.porcelainapp.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.IntentService;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.text.TextUtils;
import android.util.Log;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.rapid.porcelainapp.R;
import com.rapid.porcelainapp.constants.Constants;
import com.rapid.porcelainapp.module.ApiServiceModule;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class DashBoardPage extends AppCompatActivity {

    private FusedLocationProviderClient fusedLocationClient;
    @BindView(R.id.tv_degree)
    TextView tvDegree;

    @BindView(R.id.tv_humidity)
    TextView tvHumudity;

    @BindView(R.id.tv_km)
    TextView tvKm;

    @BindView(R.id.tv_lvl)
    TextView tvLvl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board_page);
        ButterKnife.bind(this);

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
     //   Toast.makeText(getApplicationContext(),"loc : "+fusedLocationClient.getLastLocation().getResult().toString(),Toast.LENGTH_LONG).show();
       /* fusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            // Logic to handle location object

                        }
                    }
                });

        */

       Call<ResponseBody> mService = ApiServiceModule.getInstance().getAPI().getWeather("London,uk","b6907d289e10d714a6e88b30761fae22");
        mService.enqueue(new Callback<ResponseBody>() {
            String mRegisterObject = null;
            JSONObject jsonObject = null;
            String degree;

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                JSONArray datajson;
                try {
                    if(response.isSuccessful()) {
                        mRegisterObject = response.body().string();
                        jsonObject = new JSONObject(mRegisterObject);
                        tvDegree.setText(jsonObject.getJSONObject("wind").getString("deg"));
                        tvHumudity.setText(jsonObject.getJSONObject("main").getString("humidity"));
                        tvKm.setText(jsonObject.getJSONObject("wind").getString("speed"));
                        tvLvl.setText("LVL ."+jsonObject.getJSONObject("sys").getString("type"));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                call.cancel();
            }
        });
    }

    @OnClick(R.id.iv_notif)
    void onNotificationClick(){
        Intent i = new Intent(this,InsidePage.class);
        i.putExtra("title","Notification");
        startActivity(i);
    }
    @OnClick(R.id.iv_msg)
    void onMessageClick(){
        Intent i = new Intent(this,InsidePage.class);
        i.putExtra("title","Chat");
        startActivity(i);
    }
    @OnClick(R.id.ll_products)
    void onProductsClick(){
        Intent i = new Intent(this,InsidePage.class);
        i.putExtra("title","Products");
        startActivity(i);
    }
    @OnClick(R.id.ll_treatments)
    void onTreatmentsClick(){
        Intent i = new Intent(this,InsidePage.class);
        i.putExtra("title","Treatments");
        startActivity(i);
    }
    @OnClick(R.id.ll_Appointment)
    void onAppointmentClick(){
        Intent i = new Intent(this,InsidePage.class);
        i.putExtra("title","Appointment");
        startActivity(i);
    }
}
