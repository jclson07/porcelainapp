package com.rapid.porcelainapp;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.rapid.porcelainapp.component.AppControllerComponent;
import com.rapid.porcelainapp.component.DaggerAppControllerComponent;
import com.rapid.porcelainapp.module.ApiServiceModule;
import com.rapid.porcelainapp.module.ContextModule;
import com.rapid.porcelainapp.module.PicassoModule;

public class AppController extends Application {

    private AppControllerComponent appControllerComponent;
    public static Activity context;

    public static AppController get(Activity activity) {
        context = activity;
        return (AppController) activity.getApplication();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        appControllerComponent = DaggerAppControllerComponent.builder()
                .contextModule(new ContextModule(context))
                .picassoModule(new PicassoModule())
                .apiServiceModule(new ApiServiceModule())
                .build();
    }


    public AppControllerComponent getAppControllerComponent() {
        return appControllerComponent;
    }

    public void processStatusBar(){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = context.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }

    }

    public void hideKeyboard(EditText editText){

        InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(editText.getWindowToken(), 0);

    }

}
